#!/bin/bash

# build frontend application for production (minified files)
npm run --prefix ./ng-demo-app ng -- build --prod

# copy front app distro to resources/static so it's included in JAR and visible for Spring Boot as static content
rm -R -f ./src/main/resources/static
mkdir -p ./src/main/resources/static
cp -r ./ng-demo-app/dist/ng-demo-app/* src/main/resources/static/

# build JAR
./gradlew clean build
