FROM openjdk:11.0.8-slim

RUN mkdir -p /home/app
COPY build/libs/demo-1.0.0.jar /home/app

CMD java -jar /home/app/demo-1.0.0.jar