package com.example.services.pricing;

import com.example.model.OrderItem;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Service
public class PriceCalculatorImpl implements PriceCalculator {

    @Override
    public BigDecimal calculateTotalPrice(final Collection<OrderItem> orderedItems) {
        Objects.requireNonNull(orderedItems, "Parameter 'orderedItems' must not be null!");
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (OrderItem orderedItem : orderedItems) {
            final BigDecimal price = orderedItem.getProduct().getPrice();
            if (price.compareTo(BigDecimal.ZERO) < 0) {
                throw new IllegalArgumentException("Price must not be negative! Current price: " + price);
            }
            if (orderedItem.getQuantity() < 0) {
                throw new IllegalArgumentException("Quantity must not be negative! Current quantity: " + orderedItem.getQuantity());
            }
            final BigDecimal quantity = BigDecimal.valueOf(orderedItem.getQuantity());
            totalPrice = totalPrice.add(price.multiply(quantity));
        }
        return totalPrice;
    }
}
