package com.example.services.pricing;

import com.example.model.OrderItem;

import java.math.BigDecimal;
import java.util.Collection;

public interface PriceCalculator {

    BigDecimal calculateTotalPrice(Collection<OrderItem> orderedItems);
}
