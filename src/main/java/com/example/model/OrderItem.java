package com.example.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class OrderItem {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order-item-seq")
    @SequenceGenerator(name = "order-item-seq", sequenceName = "ORDER_ITEM_SEQ", allocationSize = 1)
    @Id
    private int id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ORDER_ID")
    private Order order;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @Basic(optional = false)
    private int quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public OrderItem withId(int id) {
        this.id = id;
        return this;
    }

    public OrderItem withOrder(Order order) {
        this.order = order;
        return this;
    }

    public OrderItem withProduct(Product product) {
        this.product = product;
        return this;
    }

    public OrderItem withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return id == orderItem.id &&
                quantity == orderItem.quantity &&
                // breaking cycle dependency and for model classes it should be sufficient
                Objects.equals((order != null) ? order.getId() : 31, (orderItem.order != null) ? orderItem.order.getId() : 31) &&
                Objects.equals((product != null) ? product.getId() : 31, (orderItem.product != null) ? orderItem.product.getId() : 31);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                // breaking cycle dependency and for model classes it should be sufficient
                (order != null) ? order.getId() : 31,
                (product != null) ? product.getId() : 31,
                quantity);
    }
}
