package com.example.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "\"ORDER\"")  // "ORDER" is Postgres keyword
public class Order {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order-seq")
    @SequenceGenerator(name = "order-seq", sequenceName = "ORDER_SEQ", allocationSize = 1)
    @Id
    private int id;

    @OneToMany(mappedBy = "order", cascade = {CascadeType.PERSIST})
    private Set<OrderItem> orderItems;

    @Column(nullable = false)
    private String buyersEmail;

    @Column(nullable = false)
    private LocalDateTime orderTime;

    @Column(nullable = false)
    private BigDecimal orderTotalPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getBuyersEmail() {
        return buyersEmail;
    }

    public void setBuyersEmail(String buyersEmail) {
        this.buyersEmail = buyersEmail;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalDateTime orderTime) {
        this.orderTime = orderTime;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public Order withId(int id) {
        this.id = id;
        return this;
    }

    public Order withOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    public Order withBuyersEmail(String buyersEmail) {
        this.buyersEmail = buyersEmail;
        return this;
    }

    public Order withOrderTime(LocalDateTime orderTime) {
        this.orderTime = orderTime;
        return this;
    }

    public Order withOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                // breaking cycle dependency and for model classes it should be sufficient
                Objects.equals(
                        (orderItems != null) ? orderItems.stream().map(OrderItem::getId).collect(Collectors.toSet()) : Collections.emptySet(),
                        (order.orderItems != null) ? order.orderItems.stream().map(OrderItem::getId).collect(Collectors.toSet()) : Collections.emptySet()) &&
                Objects.equals(buyersEmail, order.buyersEmail) &&
                Objects.equals(orderTime, order.orderTime) &&
                Objects.equals(orderTotalPrice, order.orderTotalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                // breaking cycle dependency and for model classes it should be sufficient
                (orderItems != null) ? orderItems.stream().map(OrderItem::getId).collect(Collectors.toSet()) : Collections.emptySet(),
                buyersEmail, orderTime, orderTotalPrice);
    }
}
