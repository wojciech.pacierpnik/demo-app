package com.example.orders.service;

import com.example.dao.OrderRepository;
import com.example.dao.ProductRepository;
import com.example.model.Order;
import com.example.model.OrderItem;
import com.example.model.Product;
import com.example.orders.dto.getorders.GetOrdersRsp;
import com.example.orders.dto.getorders.OrderDto;
import com.example.orders.dto.getorders.OrderItemDto;
import com.example.orders.dto.neworder.NewOrderReq;
import com.example.services.pricing.PriceCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrdersServiceImpl implements OrdersService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final PriceCalculator priceCalculator;
    private final Clock clock;

    @Autowired
    public OrdersServiceImpl(
            final OrderRepository orderRepository,
            final ProductRepository productRepository,
            final PriceCalculator priceCalculator,
            final Clock clock
    ) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.priceCalculator = priceCalculator;
        this.clock = clock;
    }

    @Override
    public GetOrdersRsp getOrdersByEmail(final String buyersEmail) {
        final List<Order> orders = orderRepository.findAllByBuyersEmail(buyersEmail);

        final GetOrdersRsp rsp = new GetOrdersRsp();
        rsp.setOrders(orders.stream().map(this::toOrderDto).collect(Collectors.toList()));
        return rsp;
    }

    @Transactional
    @Override
    public int createOrder(final NewOrderReq newOrderReq) {
        final Order order = new Order();
        order.setBuyersEmail(newOrderReq.getEmail());
        order.setOrderTime(LocalDateTime.now(clock));

        final Set<OrderItem> orderItems = newOrderReq.getOrderItems().stream()
                .map(dto -> buildOrderItemModel(dto, order))
                .collect(Collectors.toSet());
        order.setOrderTotalPrice(priceCalculator.calculateTotalPrice(orderItems));
        order.setOrderItems(orderItems);

        final Order newOrder = orderRepository.save(order);
        return newOrder.getId();
    }

    private OrderDto toOrderDto(final Order order) {
        final OrderDto dto = new OrderDto();
        dto.setEmail(order.getBuyersEmail());
        dto.setOrderTime(order.getOrderTime());
        dto.setTotalCost(order.getOrderTotalPrice());
        dto.setProducts(order.getOrderItems().stream().map(this::toOrderItemDto).collect(Collectors.toList()));
        return dto;
    }

    private com.example.orders.dto.getorders.OrderItemDto toOrderItemDto(final OrderItem orderItem) {
        final OrderItemDto dto = new OrderItemDto();
        dto.setProductName(orderItem.getProduct().getName());
        dto.setQuantity(orderItem.getQuantity());
        return dto;
    }

    public OrderItem buildOrderItemModel(
            final com.example.orders.dto.neworder.OrderItemDto orderItemDto,
            final Order order
    ) {
        final OrderItem orderItem = new OrderItem();
        final Optional<Product> productOpt = productRepository.findById(orderItemDto.getProductId());
        if (productOpt.isEmpty()) {
            throw new NoSuchElementException("Product not found for ID=" + orderItemDto.getProductId());
        }
        orderItem.setProduct(productOpt.get());
        orderItem.setOrder(order);
        orderItem.setQuantity(orderItemDto.getQuantity());
        return orderItem;
    }
}
