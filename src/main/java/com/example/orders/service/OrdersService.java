package com.example.orders.service;

import com.example.orders.dto.getorders.GetOrdersRsp;
import com.example.orders.dto.neworder.NewOrderReq;

public interface OrdersService {

    GetOrdersRsp getOrdersByEmail(String buyersEmail);

    int createOrder(NewOrderReq newOrderReq);
}
