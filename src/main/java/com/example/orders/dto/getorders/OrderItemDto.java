package com.example.orders.dto.getorders;

public class OrderItemDto {

    private int quantity;
    private String productName;
    // price is not sent to the frontend because it might cause confusion if we send back new product's price and total order cost calculated in the past

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
