package com.example.orders.dto.getorders;

import java.util.List;

public class GetOrdersRsp {

    private List<OrderDto> orders;

    public List<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDto> orders) {
        this.orders = orders;
    }
}
