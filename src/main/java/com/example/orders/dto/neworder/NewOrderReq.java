package com.example.orders.dto.neworder;

import java.util.List;

public class NewOrderReq {

    private String email;
    private List<OrderItemDto> orderItems;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OrderItemDto> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemDto> orderItems) {
        this.orderItems = orderItems;
    }
}
