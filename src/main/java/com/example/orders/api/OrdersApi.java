package com.example.orders.api;

import com.example.orders.dto.getorders.GetOrdersRsp;
import com.example.orders.dto.neworder.NewOrderReq;
import com.example.orders.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class OrdersApi {

    private final OrdersService ordersService;

    @Autowired
    public OrdersApi(final OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @GetMapping("/orders")
    public GetOrdersRsp getOrderByBuyersEmail(@RequestParam("email") final String buyersEmail) {
        return ordersService.getOrdersByEmail(buyersEmail);
    }

    @PutMapping("/orders")
    public ResponseEntity<Void> createOrder(@RequestBody final NewOrderReq newOrderReq) {
        final int newOrderId = ordersService.createOrder(newOrderReq);
        return ResponseEntity.noContent()
                .header("Location", "" + newOrderId)
                .build();
    }
}
