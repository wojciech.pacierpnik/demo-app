package com.example.products.dto.getall;

import java.util.List;

public class GetAllProductsRsp {

    private List<ProductDto> products;

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }
}
