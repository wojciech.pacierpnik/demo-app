package com.example.products.api;

import com.example.products.dto.getall.GetAllProductsRsp;
import com.example.products.dto.insertnew.NewProductReq;
import com.example.products.dto.update.UpdateProductReq;
import com.example.products.dto.update.UpdateProductRsp;
import com.example.products.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ProductsApi {

    private final ProductsService productsService;

    @Autowired
    public ProductsApi(final ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/products")
    public GetAllProductsRsp getAllProducts() {
        return productsService.getAll();
    }

    @PutMapping("/products")
    public ResponseEntity<Void> insertNewProduct(@RequestBody final NewProductReq newProductReq) {
        final int newProductsId = productsService.insertNew(newProductReq);

        return ResponseEntity.noContent()
                .header("Location", "" + newProductsId)
                .build();
    }

    @PostMapping("/products/{id}")
    public ResponseEntity<UpdateProductRsp> updateProduct(
            @PathVariable final int id,
            @RequestBody final UpdateProductReq updateProductReq
    ) {
        final Optional<UpdateProductRsp> updateProductRsp = productsService.update(id, updateProductReq);
        if (updateProductRsp.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.of(updateProductRsp);
    }
}
