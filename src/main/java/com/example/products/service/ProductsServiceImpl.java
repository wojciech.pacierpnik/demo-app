package com.example.products.service;

import com.example.dao.ProductRepository;
import com.example.model.Product;
import com.example.products.dto.getall.GetAllProductsRsp;
import com.example.products.dto.getall.ProductDto;
import com.example.products.dto.insertnew.NewProductReq;
import com.example.products.dto.update.UpdateProductReq;
import com.example.products.dto.update.UpdateProductRsp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductsServiceImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public GetAllProductsRsp getAll() {
        final GetAllProductsRsp rsp = new GetAllProductsRsp();
        final List<ProductDto> products = productRepository.findAll().stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
        rsp.setProducts(products);
        return rsp;
    }

    @Transactional
    @Override
    public int insertNew(final NewProductReq newProductReq) {
        final Product newProduct = productRepository.save(mapToModel(newProductReq));
        return newProduct.getId();
    }

    @Transactional
    @Override
    public Optional<UpdateProductRsp> update(final int id, final UpdateProductReq updateProductReq) {
        final Optional<Product> productOpt = productRepository.findById(id);
        if (productOpt.isEmpty()) {
            return Optional.empty();
        }
        final Product product = productOpt.get();
        product.setName(updateProductReq.getName());
        product.setPrice(updateProductReq.getPrice());

        final Product savedProduct = productRepository.save(product);
        return Optional.of(mapToUpdateProductRsp(savedProduct));
    }

    private ProductDto mapToDto(final Product product) {
        final ProductDto dto = new ProductDto();
        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        return dto;
    }

    private Product mapToModel(final NewProductReq req) {
        final Product product = new Product();
        product.setName(req.getName());
        product.setPrice(req.getPrice());
        return product;
    }

    private UpdateProductRsp mapToUpdateProductRsp(final Product product) {
        final UpdateProductRsp dto = new UpdateProductRsp();
        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        return dto;
    }
}
