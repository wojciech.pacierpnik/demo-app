package com.example.products.service;

import com.example.products.dto.getall.GetAllProductsRsp;
import com.example.products.dto.insertnew.NewProductReq;
import com.example.products.dto.update.UpdateProductReq;
import com.example.products.dto.update.UpdateProductRsp;

import java.util.Optional;

public interface ProductsService {

    GetAllProductsRsp getAll();

    int insertNew(NewProductReq newProductReq);

    Optional<UpdateProductRsp> update(int id, UpdateProductReq updateProductReq);
}
