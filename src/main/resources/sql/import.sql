INSERT INTO "product"("id", "name", "price") VALUES (nextval('product_seq'), 'Product 1', 10.5);
INSERT INTO "product"("id", "name", "price") VALUES (nextval('product_seq'), 'Product 2', 5.25);
INSERT INTO "product"("id", "name", "price") VALUES (nextval('product_seq'), 'Product 3', 17);
INSERT INTO "product"("id", "name", "price") VALUES (nextval('product_seq'), 'Product 4', 11);
INSERT INTO "product"("id", "name", "price") VALUES (nextval('product_seq'), 'Product 5', 13.99);

INSERT INTO "order"("id", "buyers_email", "order_time", "order_total_price") VALUES (nextval('order_seq'), 'example1@mail.com', TO_TIMESTAMP('02/08/2020 10:59:00', 'DD/MM/YYYY HH24:MI:SS'), 27.5);

INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 2, currval('order_seq'), 2);
INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 1, currval('order_seq'), 3);

INSERT INTO "order"("id", "buyers_email", "order_time", "order_total_price") VALUES (nextval('order_seq'), 'example1@mail.com', TO_TIMESTAMP('03/08/2020 15:25:31', 'DD/MM/YYYY HH24:MI:SS'), 105);

INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 10, currval('order_seq'), 1);

INSERT INTO "order"("id", "buyers_email", "order_time", "order_total_price") VALUES (nextval('order_seq'), 'example2@mail.com', TO_TIMESTAMP('02/08/2020 21:11:12', 'DD/MM/YYYY HH24:MI:SS'), 285);

INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 10, currval('order_seq'), 1);
INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 10, currval('order_seq'), 3);

INSERT INTO "order"("id", "buyers_email", "order_time", "order_total_price") VALUES (nextval('order_seq'), 'example3@mail.com', TO_TIMESTAMP('05/08/2020 11:00:00', 'DD/MM/YYYY HH24:MI:SS'), 225);

INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 5, currval('order_seq'), 4);
INSERT INTO "order_item"("id", "quantity", "order_id", "product_id") VALUES (nextval('order_item_seq'), 10, currval('order_seq'), 3);

COMMIT;
