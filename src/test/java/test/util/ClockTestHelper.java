package test.util;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public final class ClockTestHelper {

    private ClockTestHelper() {
    }

    public static Clock mockTime(final LocalDateTime dateTime) {
        final ZoneId zoneId = ZoneId.of("Europe/Warsaw");
        final ZoneOffset zoneOffset = zoneId.getRules().getOffset(dateTime);
        return Clock.fixed(dateTime.toInstant(zoneOffset), zoneId);
    }
}