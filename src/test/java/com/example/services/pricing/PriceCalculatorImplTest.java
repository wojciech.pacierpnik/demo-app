package com.example.services.pricing;

import com.example.model.OrderItem;
import com.example.model.Product;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class PriceCalculatorImplTest {

    // we might also consider if quantity=0 for ordered items is valid
    // we might also consider if price=0 for ordered product is valid

    @Test
    void calculateTotalPrice_nullOrderedItemsList() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final Throwable throwable = catchThrowable(() -> priceCalculator.calculateTotalPrice(null));

        // THEN
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    @Test
    void calculateTotalPrice_illegalNegativeQuantity() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final List<OrderItem> orderedItems = new ArrayList<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(BigDecimal.TEN))
                        .withQuantity(-1)
        );

        final Throwable throwable = catchThrowable(() -> priceCalculator.calculateTotalPrice(orderedItems));

        // THEN
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void calculateTotalPrice_illegalNegativePrice() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final List<OrderItem> orderedItems = new ArrayList<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("-1")))
                        .withQuantity(10)
        );

        final Throwable throwable = catchThrowable(() -> priceCalculator.calculateTotalPrice(orderedItems));

        // THEN
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void calculateTotalPrice_emptyOrderedItemsList() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(Collections.emptyList());

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("0");
    }

    @Test
    void calculateTotalPrice_emptyOrderedItemsSet() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(Collections.emptySet());

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("0");
    }

    @Test
    void calculateTotalPrice_oneElementOrderedItemsList() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final List<OrderItem> orderedItems = new ArrayList<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(BigDecimal.TEN))
                        .withQuantity(21)
        );

        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(orderedItems);

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("210");
    }

    @Test
    void calculateTotalPrice_oneElementOrderedItemsSet() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final Set<OrderItem> orderedItems = new HashSet<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(BigDecimal.TEN))
                        .withQuantity(21)
        );

        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(orderedItems);

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("210");
    }

    @Test
    void calculateTotalPrice_multipleElementOrderedItemsList() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final List<OrderItem> orderedItems = new ArrayList<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(BigDecimal.TEN))
                        .withQuantity(21)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("9.99")))
                        .withQuantity(10)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("13.7")))
                        .withQuantity(15)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("25.50")))
                        .withQuantity(8)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("100.00")))
                        .withQuantity(4)
        );

        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(orderedItems);

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("1119.4");
    }

    @Test
    void calculateTotalPrice_multipleElementOrderedItemsSet() {
        // GIVEN
        final PriceCalculatorImpl priceCalculator = new PriceCalculatorImpl();

        // WHEN
        final Set<OrderItem> orderedItems = new HashSet<>();
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(BigDecimal.TEN))
                        .withQuantity(21)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("9.99")))
                        .withQuantity(10)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("13.7")))
                        .withQuantity(15)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("25.50")))
                        .withQuantity(8)
        );
        orderedItems.add(
                (new OrderItem())
                        .withProduct((new Product()).withPrice(new BigDecimal("100.00")))
                        .withQuantity(4)
        );

        final BigDecimal totalPrice = priceCalculator.calculateTotalPrice(orderedItems);

        // THEN
        assertThat(totalPrice).isEqualByComparingTo("1119.4");
    }
}