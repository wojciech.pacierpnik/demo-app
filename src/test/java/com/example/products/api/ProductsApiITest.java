package com.example.products.api;

import com.example.demo.DemoApplication;
import com.example.products.dto.insertnew.NewProductReq;
import com.example.products.dto.update.UpdateProductReq;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = DemoApplication.class)
@AutoConfigureMockMvc
class ProductsApiITest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAllProducts() throws Exception {
        mockMvc.perform(get("/api/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products").isArray())
                .andExpect(jsonPath("$.products", hasSize(5)));
    }

    @Test
    @DirtiesContext
    void insertNewProduct() throws Exception {
        final NewProductReq newProductReq = new NewProductReq();
        newProductReq.setName("TestProduct");
        newProductReq.setPrice(new BigDecimal("11.5"));

        final MvcResult mvcResult = mockMvc.perform(put("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newProductReq)))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        assertThat(mvcResult.getResponse().getHeader("Location")).isEqualTo("6");
    }

    @Test
    @DirtiesContext
    void updateProduct() throws Exception {
        final UpdateProductReq updateProductReq = new UpdateProductReq();
        updateProductReq.setName("TestUpdateProduct");
        updateProductReq.setPrice(new BigDecimal("99"));

        mockMvc.perform(post("/api/products/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateProductReq)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("TestUpdateProduct")))
                .andExpect(jsonPath("$.price", is(99)));
    }

    @Test
    @DirtiesContext
    void updateProduct_notExist() throws Exception {
        final UpdateProductReq updateProductReq = new UpdateProductReq();
        updateProductReq.setName("TestUpdateProduct");
        updateProductReq.setPrice(new BigDecimal("99"));

        mockMvc.perform(post("/api/products/100")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateProductReq)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}