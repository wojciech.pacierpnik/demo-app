package com.example.products.service;

import com.example.dao.ProductRepository;
import com.example.model.Product;
import com.example.products.dto.getall.GetAllProductsRsp;
import com.example.products.dto.getall.ProductDto;
import com.example.products.dto.insertnew.NewProductReq;
import com.example.products.dto.update.UpdateProductReq;
import com.example.products.dto.update.UpdateProductRsp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductsServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    private ProductsServiceImpl cut;

    @BeforeEach
    void setUp() {
        cut = new ProductsServiceImpl(productRepository);
    }

    @Test
    void getAll_multipleResults() {
        // GIVEN
        final Product product1 = new Product();
        product1.setId(1);
        product1.setName("Product 1");
        product1.setPrice(new BigDecimal("10.50"));

        final Product product2 = new Product();
        product2.setId(2);
        product2.setName("Product 2");
        product2.setPrice(new BigDecimal("6.99"));

        when(productRepository.findAll()).thenReturn(Arrays.asList(product1, product2));

        // WHEN
        final GetAllProductsRsp rsp = cut.getAll();

        // THEN
        assertThat(rsp).isNotNull();
        assertThat(rsp.getProducts())
                .extracting(ProductDto::getId, ProductDto::getName, ProductDto::getPrice)
                .containsExactlyInAnyOrder(
                        tuple(1, "Product 1", new BigDecimal("10.50")),
                        tuple(2, "Product 2", new BigDecimal("6.99"))
                );
    }

    @Test
    void getAll_noResults() {
        // GIVEN
        when(productRepository.findAll()).thenReturn(Collections.emptyList());

        // WHEN
        final GetAllProductsRsp rsp = cut.getAll();

        // THEN
        assertThat(rsp).isNotNull();
        assertThat(rsp.getProducts()).isNotNull().isEmpty();
    }

    @Test
    void insertNew() {
        final int NEW_PRODUCT_ID = 11;
        final String NEW_PRODUCT_NAME = "Test product";
        final String NEW_PRODUCT_PRICE = "11.99";

        // GIVEN
        final ArgumentCaptor<Product> newProductReqCaptor = ArgumentCaptor.forClass(Product.class);
        final Product newProduct = new Product();
        newProduct.setId(NEW_PRODUCT_ID);
        when(productRepository.save(newProductReqCaptor.capture())).thenReturn(newProduct);

        // WHEN
        final NewProductReq newProductReq = new NewProductReq();
        newProductReq.setName(NEW_PRODUCT_NAME);
        newProductReq.setPrice(new BigDecimal(NEW_PRODUCT_PRICE));

        int newProductId = cut.insertNew(newProductReq);

        // THEN
        assertThat(newProductReqCaptor.getValue().getId()).isZero();
        assertThat(newProductReqCaptor.getValue().getName()).isEqualTo(NEW_PRODUCT_NAME);
        assertThat(newProductReqCaptor.getValue().getPrice()).isEqualByComparingTo(NEW_PRODUCT_PRICE);

        assertThat(newProductId).isEqualTo(NEW_PRODUCT_ID);
    }

    @Test
    void update_notExistent() {
        // GIVEN
        when(productRepository.findById(eq(2))).thenReturn(Optional.empty());

        // WHEN
        Optional<UpdateProductRsp> rsp = cut.update(2, new UpdateProductReq());

        // THEN
        assertThat(rsp).isEmpty();
    }

    @Test
    void update() {
        // GIVEN
        final Product originalProduct = new Product();
        originalProduct.setId(2);
        originalProduct.setName("ORIGINAL PRODUCT");
        originalProduct.setPrice(new BigDecimal("11.00"));
        when(productRepository.findById(eq(2))).thenReturn(Optional.of(originalProduct));

        final ArgumentCaptor<Product> updatedProductCaptor = ArgumentCaptor.forClass(Product.class);
        final Product updatedProduct = new Product();
        updatedProduct.setId(2);
        updatedProduct.setName("UPDATED PRODUCT");
        updatedProduct.setPrice(new BigDecimal("22.00"));
        when(productRepository.save(updatedProductCaptor.capture())).thenReturn(updatedProduct);

        // WHEN
        final UpdateProductReq updateProductReq = new UpdateProductReq();
        updateProductReq.setName("UPDATED PRODUCT");
        updateProductReq.setPrice(new BigDecimal("22.00"));
        final Optional<UpdateProductRsp> rsp = cut.update(2, updateProductReq);

        // THEN
        assertThat(updatedProductCaptor.getValue().getId()).isEqualTo(2);
        assertThat(updatedProductCaptor.getValue().getName()).isEqualTo("UPDATED PRODUCT");
        assertThat(updatedProductCaptor.getValue().getPrice()).isEqualByComparingTo("22.00");

        assertThat(rsp).isNotEmpty();
        assertThat(rsp.get().getId()).isEqualTo(2);
        assertThat(rsp.get().getName()).isEqualTo("UPDATED PRODUCT");
        assertThat(rsp.get().getPrice()).isEqualByComparingTo("22.00");
    }
}