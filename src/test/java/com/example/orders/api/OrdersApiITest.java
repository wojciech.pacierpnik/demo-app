package com.example.orders.api;

import com.example.demo.DemoApplication;
import com.example.orders.dto.neworder.NewOrderReq;
import com.example.orders.dto.neworder.OrderItemDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = DemoApplication.class)
@AutoConfigureMockMvc
class OrdersApiITest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getOrderByBuyersEmail() throws Exception {
        mockMvc.perform(get("/api/orders?email=example1@mail.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").isArray())
                .andExpect(jsonPath("$.orders", hasSize(2)));
    }

    @Test
    void getOrderByBuyersEmail_notExistentMail() throws Exception {
        mockMvc.perform(get("/api/orders?email=notExist@mail.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").isArray())
                .andExpect(jsonPath("$.orders", hasSize(0)));
    }

    @Test
    @DirtiesContext
    void createOrder() throws Exception {
        final NewOrderReq newOrderReq = new NewOrderReq();
        newOrderReq.setEmail("orderTest@mail.com");
        final OrderItemDto orderItemDto = new OrderItemDto();
        orderItemDto.setProductId(1);
        orderItemDto.setQuantity(2);
        newOrderReq.setOrderItems(Collections.singletonList(orderItemDto));

        final MvcResult mvcResult = mockMvc.perform(put("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newOrderReq)))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        assertThat(mvcResult.getResponse().getHeader("Location")).isEqualTo("5");
    }
}