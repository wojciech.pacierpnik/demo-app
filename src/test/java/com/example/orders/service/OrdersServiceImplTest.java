package com.example.orders.service;

import com.example.dao.OrderRepository;
import com.example.dao.ProductRepository;
import com.example.model.Order;
import com.example.model.OrderItem;
import com.example.model.Product;
import com.example.orders.dto.getorders.GetOrdersRsp;
import com.example.orders.dto.neworder.NewOrderReq;
import com.example.orders.dto.neworder.OrderItemDto;
import com.example.services.pricing.PriceCalculator;
import com.example.services.pricing.PriceCalculatorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import test.util.ClockTestHelper;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrdersServiceImplTest {

    private static final LocalDateTime CLOCK_FIXED_TIME = LocalDateTime.of(2020, Month.SEPTEMBER, 4, 9, 58, 30);

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductRepository productRepository;

    private PriceCalculator priceCalculator;
    private Clock clock;

    private OrdersServiceImpl cut;

    @BeforeEach
    void setUp() {
        clock = ClockTestHelper.mockTime(CLOCK_FIXED_TIME);
        priceCalculator = new PriceCalculatorImpl();
        cut = new OrdersServiceImpl(orderRepository, productRepository, priceCalculator, clock);
    }

    @Test
    void getOrdersByEmail_empty() {
        // GIVEN
        when(orderRepository.findAllByBuyersEmail(eq("example1@mail.com"))).thenReturn(Collections.emptyList());

        // WHEN
        final GetOrdersRsp rsp = cut.getOrdersByEmail("example1@mail.com");

        // THEN
        assertThat(rsp).isNotNull();
        assertThat(rsp.getOrders()).isEmpty();
    }

    @Test
    void getOrdersByEmail() {
        // GIVEN
        final Product product = new Product();
        product.setId(1);
        product.setName("Product 1");
        product.setPrice(new BigDecimal("10.5"));

        final Order order = new Order();
        order.setId(1);
        order.setBuyersEmail("example1@mail.com");
        order.setOrderTime(LocalDateTime.of(2020, Month.AUGUST, 15, 17, 30, 21));
        order.setOrderTotalPrice(new BigDecimal("105"));

        final OrderItem orderItem = new OrderItem();
        orderItem.setId(1);
        orderItem.setOrder(order);
        orderItem.setProduct(product);
        orderItem.setQuantity(10);

        final HashSet<OrderItem> orderItems = new HashSet<>();
        orderItems.add(orderItem);
        order.setOrderItems(orderItems);

        final List<Order> orders = Collections.singletonList(order);
        when(orderRepository.findAllByBuyersEmail(eq("example1@mail.com"))).thenReturn(orders);

        // WHEN
        final GetOrdersRsp rsp = cut.getOrdersByEmail("example1@mail.com");

        // THEN
        assertThat(rsp).isNotNull();
        assertThat(rsp.getOrders()).hasSize(1);
        assertThat(rsp.getOrders().get(0).getEmail()).isEqualTo("example1@mail.com");
        assertThat(rsp.getOrders().get(0).getOrderTime()).isEqualTo(LocalDateTime.of(2020, Month.AUGUST, 15, 17, 30, 21));
        assertThat(rsp.getOrders().get(0).getTotalCost()).isEqualByComparingTo("105");
        assertThat(rsp.getOrders().get(0).getProducts()).hasSize(1);
        assertThat(rsp.getOrders().get(0).getProducts().get(0).getProductName()).isEqualTo("Product 1");
        assertThat(rsp.getOrders().get(0).getProducts().get(0).getQuantity()).isEqualTo(10);
    }

    @Test
    void createOrder() {
        final String BUYERS_EMAIL = "orderTest@mail.com";
        final int NEW_ORDER_ID = 7;
        final int PRODUCT_ID = 1;
        final BigDecimal PRODUCT_PRICE = BigDecimal.TEN;

        // GIVEN
        final ArgumentCaptor<Order> orderArgumentCaptor = ArgumentCaptor.forClass(Order.class);
        when(orderRepository.save(orderArgumentCaptor.capture())).thenReturn((new Order()).withId(NEW_ORDER_ID));

        final ArgumentCaptor<Integer> productIdCaptor = ArgumentCaptor.forClass(Integer.class);
        when(productRepository.findById(productIdCaptor.capture()))
                .thenReturn(Optional.of(
                        (new Product())
                                .withId(PRODUCT_ID)
                                .withPrice(PRODUCT_PRICE)
                ));

        // WHEN
        final NewOrderReq newOrderReq = new NewOrderReq();
        newOrderReq.setEmail(BUYERS_EMAIL);
        final OrderItemDto orderItemDto = new OrderItemDto();
        orderItemDto.setProductId(PRODUCT_ID);
        orderItemDto.setQuantity(2);
        newOrderReq.setOrderItems(Collections.singletonList(orderItemDto));
        int newOrderId = cut.createOrder(newOrderReq);

        // THEN
        assertThat(productIdCaptor.getValue()).isEqualTo(PRODUCT_ID);

        assertThat(orderArgumentCaptor.getValue()).isNotNull();
        assertThat(orderArgumentCaptor.getValue().getId()).isZero();
        assertThat(orderArgumentCaptor.getValue().getBuyersEmail()).isEqualTo(BUYERS_EMAIL);
        assertThat(orderArgumentCaptor.getValue().getOrderTotalPrice()).isEqualByComparingTo("20");
        assertThat(orderArgumentCaptor.getValue().getOrderTime()).isEqualTo(CLOCK_FIXED_TIME);
        assertThat(orderArgumentCaptor.getValue().getOrderItems()).hasSize(1);

        final OrderItem capturedOrderItem = orderArgumentCaptor.getValue().getOrderItems().iterator().next();
        assertThat(capturedOrderItem.getId()).isZero();
        assertThat(capturedOrderItem.getOrder()).isSameAs(orderArgumentCaptor.getValue());
        assertThat(capturedOrderItem.getProduct().getId()).isEqualTo(PRODUCT_ID);
        assertThat(capturedOrderItem.getProduct().getPrice()).isEqualTo(PRODUCT_PRICE);

        assertThat(newOrderId).isEqualTo(NEW_ORDER_ID);
    }

    @Test
    void createOrder_notExistingProductId() {
        final String BUYERS_EMAIL = "orderTest@mail.com";
        final int PRODUCT_ID = 100;

        // GIVEN
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());

        // WHEN
        final NewOrderReq newOrderReq = new NewOrderReq();
        newOrderReq.setEmail(BUYERS_EMAIL);
        final OrderItemDto orderItemDto = new OrderItemDto();
        orderItemDto.setProductId(PRODUCT_ID);
        orderItemDto.setQuantity(2);
        newOrderReq.setOrderItems(Collections.singletonList(orderItemDto));
        final Throwable throwable = catchThrowable(() -> cut.createOrder(newOrderReq));

        // THEN
        assertThat(throwable).isInstanceOf(NoSuchElementException.class);
    }
}