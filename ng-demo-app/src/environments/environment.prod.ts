export const environment = {
  production: true,
  server: {
    // 'url' should be publicly available address (not localhost!), but for demo purpose localhost is sufficient (I don't have hosting and alias for exposing application publicly)
    url: "http://localhost:8080"
  }
};
