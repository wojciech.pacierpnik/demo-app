import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../product";
import {ProductsService} from "../service/products.service";
import {NewProductReq, UpdateProductReq} from "../service/io/types";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Input() public product: Product;
  @Output() public onCancel: EventEmitter<any> = new EventEmitter();
  @Output() public onConfirm: EventEmitter<any> = new EventEmitter();

  constructor(private productsService: ProductsService) {
  }

  ngOnInit(): void {
  }

  public cancelEdit() {
    this.onCancel.emit(null);
  }

  public confirmEdit() {
//     console.log("Confirmed with product state: %s", JSON.stringify(this.product))
    if (this.product.isNewProductPlaceholder) {
      this.insertNewProduct(this.product);
    } else {
      this.updateProduct(this.product);
    }
  }

  private updateProduct(product: Product) {
    let req: UpdateProductReq = {
      name: product.name,
      price: product.price
    };

    this.productsService.updateProduct(product.id, req)
      .subscribe(updateProductRsp => this.onConfirm.emit(updateProductRsp));
  }

  private insertNewProduct(product: Product) {
    let req: NewProductReq = {
      name: product.name,
      price: product.price
    };

    this.productsService.insertNewProduct(req)
      .subscribe(newProductRsp => this.onConfirm.emit(newProductRsp.headers.get('Location')));
  }
}
