export interface Product {
  id?: number;
  name: string;
  price: number;

  isNewProductPlaceholder?: boolean;
  isEdited?: boolean;
}
