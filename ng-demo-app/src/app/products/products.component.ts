import {Component, OnInit} from '@angular/core';
import {Product} from "./product";
import {ProductsService} from "./service/products.service";

const PRODUCTS_IN_ROW = 3;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  productsMatrix: Product[][] = [];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit(): void {
    this.fetchProducts();
  }

  public edit(product: Product) {
//     console.log("Edit product with id=%s", product.id)
    product.isEdited = true;
  }

  public onCancelEdit(product: Product) {
    product.isEdited = false;
  }

  public onConfirmEdit(product: Product) {
    product.isEdited = false;
    this.fetchProducts();
  }

  private fetchProducts() {
    this.productsService.getAllProducts()
      .subscribe(
        products => this.postProcessProducts(products),
        error => this.onError(error)
      );
  }

  private postProcessProducts(products: Product[]) {
    products.push({name: '', price: 0, isNewProductPlaceholder: true});
    this.productsMatrix = ProductsComponent.toMatrix(products);
  }

  private static toMatrix(products: Product[], cols: number = PRODUCTS_IN_ROW): Product[][] {
    let matrix: Product[][] = [];
    for (let i = 0; i < products.length; i += cols) {
      console.log("Idx: %s", i)
      matrix.push(products.slice(i, i + cols));
    }
    console.log(JSON.stringify(matrix));
    return matrix;
  }

  private onError(error) {
    console.log(error);
  }
}
