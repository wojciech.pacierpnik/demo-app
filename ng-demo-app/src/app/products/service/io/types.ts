export interface GetAllProductsRsp {
  products: object[];
}

export interface UpdateProductReq {
  name: string,
  price: number
}

export interface UpdateProductRsp {
  id: number,
  name: string,
  price: number
}

export interface NewProductReq {
  name: string,
  price: number
}
