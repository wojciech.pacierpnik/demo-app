import {Product} from "../product";

export const EXAMPLE_PRODUCTS: Product[] = [
  {name: 'Product 1', price: 10.4},
  {name: 'Product 2', price: 31.21},
  {name: 'Product 3', price: 11.99},
  {name: 'Product 4', price: 5.99},
];
