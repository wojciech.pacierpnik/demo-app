import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../product";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {GetAllProductsRsp, NewProductReq, UpdateProductReq, UpdateProductRsp} from "./io/types";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient) {
  }

  public getAllProducts(): Observable<Product[]> {
    return this.httpClient.get<GetAllProductsRsp>(`${environment.server.url}/api/products`)
      .pipe(
        map(rsp => rsp.products.map(this.mapToProduct))
      );
  }

  public updateProduct(id: number, updateProductReq: UpdateProductReq): Observable<UpdateProductRsp> {
    return this.httpClient.post<UpdateProductRsp>(`${environment.server.url}/api/products/${id}`, updateProductReq);
  }

  public insertNewProduct(newProductReq: NewProductReq): Observable<HttpResponse<any>> {
    return this.httpClient.put(`${environment.server.url}/api/products`, newProductReq, { observe: 'response' });
  }

  private mapToProduct(productDto): Product {
    return ({
      id: productDto.id,
      name: productDto.name,
      price: productDto.price
    });
  }
}
