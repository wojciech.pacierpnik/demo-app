export interface Order {
  email: string;
  orderTime: Date;
  totalCost: number;
  products: OrderItem[];
}

export interface OrderItem {
  quantity: number;
  productName: string;
}
