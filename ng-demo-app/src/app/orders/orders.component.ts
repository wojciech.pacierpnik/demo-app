import { Component, OnInit } from '@angular/core';
import { OrdersService } from "./service/orders.service"
import { Order } from "./order"

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  public searchedEmail: string;
  orders: Order[];

  constructor(private ordersService: OrdersService) { }

  ngOnInit(): void {
  }

  public search() {
    this.ordersService.getOrdersByEmail(this.searchedEmail)
      .subscribe(
        orders => this.orders = orders,
        error => this.onError(error)
      );
  }

  private onError(error) {
    console.log(error);
  }
}
