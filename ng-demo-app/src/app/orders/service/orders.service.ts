import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Order} from "../order";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {GetOrdersRsp, NewOrderReq} from "./io/types";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private httpClient: HttpClient) { }

  public getOrdersByEmail(email: string): Observable<Order[]> {
    let params = new HttpParams().set("email", email);

    return this.httpClient.get<GetOrdersRsp>(`${environment.server.url}/api/orders`, { params: params })
      .pipe(
        map(rsp => rsp.orders.map(this.mapToOrder))
      );
  }

  public createOrder(newOrderReq: NewOrderReq): Observable<HttpResponse<any>> {
    return this.httpClient.put(`${environment.server.url}/api/orders`, newOrderReq, { observe: 'response' });
  }

  private mapToOrder(orderDto): Order {
    return ({...orderDto});
//     return ({
//       email: orderDto.email,
//       orderTime: orderDto.orderTime,
//       totalCost: orderDto.totalCost,
//       products: orderDto.products.map()
//     });
  }
}
