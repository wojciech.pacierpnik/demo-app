import {Order} from "../order";

export const EXAMPLE_ORDERS: Order[] = [
  {
    email: "example1@mail.com",
    orderTime: new Date(),
    totalCost: 27.50,
    products: [
      { productName: "Product 2", quantity: 2 },
      { productName: "Product 3", quantity: 1 },
    ]
  },
  {
    email: "example1@mail.com",
    orderTime: new Date(),
    totalCost: 105.00,
    products: [
      { productName: "Product 1", quantity: 10 },
    ]
  }
];
