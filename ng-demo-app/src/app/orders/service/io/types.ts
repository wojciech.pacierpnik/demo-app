export interface GetOrdersRsp {
  orders: OrderDto[];
}

export interface OrderDto {
  email: string;
  orderTime: Date;
  totalCost: number;
  products: OrderItemDto[];
}

export interface OrderItemDto {
  quantity: number;
  productName: string;
}

export interface NewOrderReq {
  email: string;
  orderItems: NewOrderItemDto[];
}

export interface NewOrderItemDto {
  productId: number;
  quantity: number;
}
