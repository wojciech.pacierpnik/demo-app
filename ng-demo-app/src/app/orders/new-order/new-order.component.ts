import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../products/service/products.service";
import {OrdersService} from "../service/orders.service";
import {Product} from "../../products/product";
import {NewOrderReq} from "../service/io/types";

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {

  public products: Product[] = [];
  public order: NewOrderReq = { email: "", orderItems: [] };

  public selectedProductId: number;
  public inputQuantity: number;

  constructor(
    private productsService: ProductsService,
    private ordersService: OrdersService
  ) { }

  ngOnInit(): void {
    this.fetchAvailableProducts();
  }

  public confirm() {
    this.ordersService.createOrder(this.order)
      .subscribe(
        rsp => { console.log("Order created!"); this.order = { email: "", orderItems: [] }; },
        error => this.onError(error));
  }

  public addOrderItem() {
    this.order.orderItems.push( { productId: this.selectedProductId, quantity: this.inputQuantity } );

    this.selectedProductId = null;
    this.inputQuantity = null;
  }

  // not very efficient solution
  public getProductById(id: number): Product {
    return this.products.find(p => p.id === id);
  }

  private fetchAvailableProducts() {
    this.productsService.getAllProducts()
      .subscribe(products => this.products = products);
  }

  private onError(error) {
    console.log(error);
  }
}
