import {Component} from '@angular/core';
import {MenuItem} from "./menu-item";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-demo-app';

  selectedMenuItem: number = 0;
  menuItems: MenuItem[] = [
    {title: 'Home', url: '/home'},
    {title: 'Products', url: '/products'},
    {title: 'Orders', url: '/orders'}
  ];

  public isMenuItemSelected(menuItemNo: number): boolean {
    return (menuItemNo == this.selectedMenuItem);
  }

  public selectItem(menuItemNo: number) {
    this.selectedMenuItem = menuItemNo;
  }
}
