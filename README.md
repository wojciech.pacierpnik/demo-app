# Introduction

This document describes building and running of 'demo' application.  

Assumptions being made:
- Java 11 is installed and configured (i.e. is on PATH)
- commands and scripts are written under linux
- you have docker installed and run
- you are not behind any proxy or have it configured properly (for gradle and docker)
- project use gradle for dependency management and building

## Disclaimer

This 'demo' application is only prototype so there are used some coding practices 
that are not respected for production-grade software, but used here to simplify and make development faster.
One of such practices is hardcoding and not encrypting passwords. 
The other is letting Hibernate create and drop the database.  
From the security perspective Product and Order models has ID column implemented as sequentially generated integers and 
those IDs are returned from API which creates potential vulnerability - potential attacker may enumerate all data from API. 

## Building

Open terminal in project's root catalog then execute: `.\gradlew clean build`.

### JAR version management

If `version` in `build.gradle` changes, it should be also adjusted in `Dockerfile` which points to concrete JAR name.

## Running application

You can run application in two modes: `local` and `dev`.
For `local` mode you run database and application separately, 
whereas for `dev` you run whole docker-compose environment with one command.

Unless you want simpler possibility to access directly database with some database client (for example `psql`)
suggested running mode is `dev`.

Before you run application, remember to build it with `gradle`. 
It is especially important to build it before running in `dev` mode, 
because application's docker image is built based on the built JAR.  

### Local mode

1. Go to `application.properties` and set the only active profile to `local`: `spring.profiles.active=local`.
2. Open terminal, go to project's root and execute `run_local_db.sh`.
3. Run application.

To shut down application and database you have to do it manually. 
For the database instance you must kill (and remove) the docker's container.

### Dev mode

1. Go to `application.properties` and set the only active profile to `dev`: `spring.profiles.active=dev`.
2. Open terminal, go to project's root and execute `docker-compose up --build`.

The `--build` option in `up` command is only required when fresh JAR is built.  
To shut down docker-compose environment execute: `docker-compose down`. 

## Testing

For manual testing, there is Postman's collection placed in directory `postman/` under root of the project.
