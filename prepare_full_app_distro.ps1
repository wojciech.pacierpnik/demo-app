# build frontend application for production (minified files)
npm run --prefix ./ng-demo-app ng -- build --prod

# copy front app distro to resources/static so it's included in JAR and visible for Spring Boot as static content
Remove-Item -Path "src\main\resources\static" -Recurse -ErrorAction SilentlyContinue
New-Item "src\main\resources\static" -ItemType Directory
Copy-Item -Path "ng-demo-app\dist\ng-demo-app\*" -Destination "src\main\resources\static" -Recurse

# build JAR
./gradlew clean build
